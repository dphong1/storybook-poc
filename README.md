# storybook-poc-2

> 

[![NPM](https://img.shields.io/npm/v/storybook-poc-2.svg)](https://www.npmjs.com/package/storybook-poc-2) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save storybook-poc-2
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'storybook-poc-2'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [dphong1995](https://github.com/dphong1995)
