# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.18.8](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.7...v1.18.8) (2019-10-14)


### Bug Fixes

* test ([052c1cb](https://github.com/dphong1995/storybook-poc-2/commit/052c1cb))

### [1.18.7](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.6...v1.18.7) (2019-10-14)


### Bug Fixes

* test ([53cb457](https://github.com/dphong1995/storybook-poc-2/commit/53cb457))

### [1.18.6](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.5...v1.18.6) (2019-10-12)


### Bug Fixes

* test ([6765b21](https://github.com/dphong1995/storybook-poc-2/commit/6765b21))

### [1.18.5](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.4...v1.18.5) (2019-10-07)

### [1.18.4](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.3...v1.18.4) (2019-10-07)

### [1.18.3](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.2...v1.18.3) (2019-09-30)


### Bug Fixes

* new lines ([9f7aef2](https://github.com/dphong1995/storybook-poc-2/commit/9f7aef2))

### [1.18.2](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.1...v1.18.2) (2019-09-30)


### Bug Fixes

* commit all ([c4ee404](https://github.com/dphong1995/storybook-poc-2/commit/c4ee404))

### [1.18.2](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.1...v1.18.2) (2019-09-30)


### Bug Fixes

* commit all ([c4ee404](https://github.com/dphong1995/storybook-poc-2/commit/c4ee404))

### [1.18.1](https://github.com/dphong1995/storybook-poc-2/compare/v1.18.0...v1.18.1) (2019-09-30)

## [1.18.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.3...v1.18.0) (2019-09-30)


### Features

* release ([5f202bd](https://github.com/dphong1995/storybook-poc-2/commit/5f202bd))

### [1.17.3](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.2...v1.17.3) (2019-09-30)

### [1.17.2](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.1...v1.17.2) (2019-09-30)

### [1.17.1](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.0...v1.17.1) (2019-09-30)

## [1.17.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.16.0...v1.17.0) (2019-09-30)


### Features

* release ([92c4aa4](https://github.com/dphong1995/storybook-poc-2/commit/92c4aa4))
* remove redundant line ([0c994bd](https://github.com/dphong1995/storybook-poc-2/commit/0c994bd))
* remove redundant line ([ee1d306](https://github.com/dphong1995/storybook-poc-2/commit/ee1d306))

## [1.18.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.1...v1.18.0) (2019-09-30)


### Features

* remove redundant line ([0c994bd](https://github.com/dphong1995/storybook-poc-2/commit/0c994bd))

## [1.18.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.1...v1.18.0) (2019-09-30)


### Features

* remove redundant line ([0c994bd](https://github.com/dphong1995/storybook-poc-2/commit/0c994bd))

## [1.18.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.1...v1.18.0) (2019-09-30)


### Features

* remove redundant line ([0c994bd](https://github.com/dphong1995/storybook-poc-2/commit/0c994bd))

### [1.17.1](https://github.com/dphong1995/storybook-poc-2/compare/v1.17.0...v1.17.1) (2019-09-30)

## [1.17.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.16.0...v1.17.0) (2019-09-30)


### Features

* remove redundant line ([ee1d306](https://github.com/dphong1995/storybook-poc-2/commit/ee1d306))

## [1.16.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.15.0...v1.16.0) (2019-09-27)


### Features

* remove redundant line ([71846e6](https://github.com/dphong1995/storybook-poc-2/commit/71846e6))

## [1.15.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.14.0...v1.15.0) (2019-09-27)


### Features

* remove redundant line ([7d64d74](https://github.com/dphong1995/storybook-poc-2/commit/7d64d74))
* remove redundant line ([3c0b7d0](https://github.com/dphong1995/storybook-poc-2/commit/3c0b7d0))

## [1.14.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.13.0...v1.14.0) (2019-09-27)


### Features

* remove redundant line ([bb5a2d5](https://github.com/dphong1995/storybook-poc-2/commit/bb5a2d5))

## [1.13.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.12.0...v1.13.0) (2019-09-27)


### Features

* remove redundant line ([c9fc8cb](https://github.com/dphong1995/storybook-poc-2/commit/c9fc8cb))

## [1.12.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.11.0...v1.12.0) (2019-09-27)


### Features

* remove redundant line ([e429b5a](https://github.com/dphong1995/storybook-poc-2/commit/e429b5a))

## [1.11.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.10.0...v1.11.0) (2019-09-27)


### Features

* remove redundant line ([142c668](https://github.com/dphong1995/storybook-poc-2/commit/142c668))

## [1.10.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.9.0...v1.10.0) (2019-09-27)


### Features

* remove redundant line ([189fe8f](https://github.com/dphong1995/storybook-poc-2/commit/189fe8f))

## [1.9.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.8.0...v1.9.0) (2019-09-27)


### Features

* remove redundant line ([48c4750](https://github.com/dphong1995/storybook-poc-2/commit/48c4750))

## [1.8.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.7.0...v1.8.0) (2019-09-27)


### Features

* remove redundant line ([80327c5](https://github.com/dphong1995/storybook-poc-2/commit/80327c5))

## [1.7.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.6.0...v1.7.0) (2019-09-27)


### Features

* remove redundant line ([ae98094](https://github.com/dphong1995/storybook-poc-2/commit/ae98094))

## [1.6.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.5.0...v1.6.0) (2019-09-27)


### Features

* remove redundant line ([ea0d5a2](https://github.com/dphong1995/storybook-poc-2/commit/ea0d5a2))

## [1.5.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.4.0...v1.5.0) (2019-09-27)


### Features

* remove redundant line ([b6bf873](https://github.com/dphong1995/storybook-poc-2/commit/b6bf873))

## [1.4.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.3.0...v1.4.0) (2019-09-27)


### Features

* remove redundant line ([8859451](https://github.com/dphong1995/storybook-poc-2/commit/8859451))

## [1.3.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.2.0...v1.3.0) (2019-09-27)


### Features

* remove redundant line ([13efec6](https://github.com/dphong1995/storybook-poc-2/commit/13efec6))

## [1.2.0](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.24...v1.2.0) (2019-09-27)


### Features

* remove redundant line ([b0cfc2c](https://github.com/dphong1995/storybook-poc-2/commit/b0cfc2c))

### [1.1.24](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.23...v1.1.24) (2019-09-27)


### Bug Fixes

* remove redundant line ([2eecea8](https://github.com/dphong1995/storybook-poc-2/commit/2eecea8))

### [1.1.23](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.22...v1.1.23) (2019-09-27)


### Bug Fixes

* 123456 ([847b279](https://github.com/dphong1995/storybook-poc-2/commit/847b279))
* 123456 ([4ed08ba](https://github.com/dphong1995/storybook-poc-2/commit/4ed08ba))

### [1.1.8](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.9...v1.1.8) (2019-09-26)

### [1.1.7](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.6...v1.1.7) (2019-09-26)

### [1.1.8](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.7...v1.1.8) (2019-09-26)


### Bug Fixes

* 123 ([f1df9a0](https://github.com/dphong1995/storybook-poc-2/commit/f1df9a0))
* 123 ([c5cea7d](https://github.com/dphong1995/storybook-poc-2/commit/c5cea7d))

### [1.1.7](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.6...v1.1.7) (2019-09-26)


### Bug Fixes

* 123 ([0d2f5fe](https://github.com/dphong1995/storybook-poc-2/commit/0d2f5fe))
* 123 ([091f4d0](https://github.com/dphong1995/storybook-poc-2/commit/091f4d0))

### [1.1.6](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.5...v1.1.6) (2019-09-26)


### Bug Fixes

* 123 ([99657f1](https://github.com/dphong1995/storybook-poc-2/commit/99657f1))
* 123 ([a6b467c](https://github.com/dphong1995/storybook-poc-2/commit/a6b467c))

### [1.1.5](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.3...v1.1.5) (2019-09-26)


### Bug Fixes

* 123 ([60d66d6](https://github.com/dphong1995/storybook-poc-2/commit/60d66d6))

### [1.1.3](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.1...v1.1.3) (2019-09-26)


### Bug Fixes

* 123 ([661ff6a](https://github.com/dphong1995/storybook-poc-2/commit/661ff6a))

### [1.1.2](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.1...v1.1.2) (2019-09-26)


### Bug Fixes

* 123 ([661ff6a](https://github.com/dphong1995/storybook-poc-2/commit/661ff6a))

### [1.1.1](https://github.com/dphong1995/storybook-poc-2/compare/v1.1.0...v1.1.1) (2019-09-20)


### Bug Fixes

* yarn.lock ([6392c7b](https://github.com/dphong1995/storybook-poc-2/commit/6392c7b))

## 1.1.0 (2019-09-20)


### Features

* enforce commit ([8c12f25](https://github.com/dphong1995/storybook-poc-2/commit/8c12f25))
* enforce commit ([2a1710c](https://github.com/dphong1995/storybook-poc-2/commit/2a1710c))
