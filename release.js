const fs = require('fs')
const path = require('path')
const bump = require('standard-version/lib/lifecycles/bump')
const changelog = require('standard-version/lib/lifecycles/changelog')
const latestSemverTag = require('standard-version/lib/latest-semver-tag')
const commit = require('standard-version/lib/lifecycles/commit')
const tag = require('standard-version/lib/lifecycles/tag')
const runExec = require('standard-version/lib/run-exec')

let pkgPath = path.resolve(process.cwd(), 'package.json')
let data = fs.readFileSync(pkgPath, 'utf8')
const pkg = JSON.parse(data)
let newVersion
let defaults = require('standard-version/defaults')
let args = Object.assign({}, defaults, {releaseCommitMessageFormat: '[skip ci] release {{currentTag}}', verify: false, commitAll: true})
return Promise.resolve().then(() => latestSemverTag())
  .then(version => { newVersion = version })
  .then(() => bump(args, newVersion))
  .then((_newVersion) => {
    if (_newVersion) newVersion = _newVersion
    pkg.version = newVersion
    fs.writeFileSync(pkgPath, JSON.stringify(pkg, null, 2))
    return changelog(args, newVersion)
  })
  .then(() => runExec(args, `git commit --no-verify -am "[skip ci] release ${newVersion}"`))
  .then(() => tag(newVersion, pkg, args))
