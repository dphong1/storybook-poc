import {configure} from '@storybook/react'
import {addDecorator} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'

addDecorator(withInfo)
// automatically import all files ending in *.stories.js
configure(require.context('../src/stories', true, /\.stories\.js$/), module)
