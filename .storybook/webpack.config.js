module.exports = async ({config}) => {
  const cssRule = config.module.rules.find((rule) => rule.test.toString() === "/\\.css$/");
  const cssLoader = cssRule.use.find((loader) => new RegExp('\/css-loader\/').test(loader.loader));
  cssLoader.options.modules = true;
  return config;
};
